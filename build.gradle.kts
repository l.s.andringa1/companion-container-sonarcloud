import nl.rug.digitallab.themis.companion.buildsrc.buildintegration.BuildIntegrationTask
import nl.rug.digitallab.themis.companion.buildsrc.runintegration.RunIntegrationTask

val projectGroupId: String by project
val judgementCommunicationSpecVersion: String by rootProject
val protobufVersion: String by rootProject

plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
    id("org.sonarqube") version "4.4.1.3373"
}

sonar {
  properties {
    property("sonar.projectKey", "55950779_lars-personal")
    property("sonar.organization", "55950779")
    property("sonar.coverage.jacoco.xmlReportPaths", "**/build/jacoco-report/jacoco.xml")
  }
}

subprojects {
    group = projectGroupId

    apply {
        plugin("nl.rug.digitallab.gradle.plugin.quarkus.project")
    }

    dependencies {
        implementation("nl.rug.digitallab.themis.judgement:judgement-communication-spec:$judgementCommunicationSpecVersion")
    }
}

tasks.register<Copy>("prepareIntegrationBuilding") {
    group = "digital lab"
    description = "Copies the necessary files to the build directory"
    from(
        "integration-tester/environment/Dockerfile",
        "integration-tester/environment/entrypoint.sh",
        "integration-tester/environment/compose.yaml",
        "integration-tester/environment/runsc-docker.json",
        "integration-tester/environment/monitor-syscalls.json",
    )
    into("/companion-container/build/docker")
}

// Register the custom gradle tasks
tasks.register<BuildIntegrationTask>("buildIntegration") {
    group = "digital lab"
    description = "Builds the docker image used for integration testing"
    dependsOn(
        "prepareIntegrationBuilding",
        "companion-container:build",
        "integration-tester:build",
    )

    dockerfile = File(project(":companion-container").layout.buildDirectory.asFile.get().path + "/docker/Dockerfile")
    srcDir = File(project(":companion-container").layout.buildDirectory.asFile.get().path + "/docker")
}

tasks.register<RunIntegrationTask>("runIntegration") {
    group = "digital lab"
    description = "Runs the docker image to perform integration testing"
    dependsOn("buildIntegration")

    companionBuild = File(project(":companion-container").layout.buildDirectory.asFile.get().path + "/quarkus-app")
    testerBuild = File(project(":integration-tester").layout.buildDirectory.asFile.get().path + "/quarkus-app")
    tests = File(project(":integration-tester").layout.projectDirectory.asFile.path + "/tests")
}
