#include <unistd.h>

int main() {
    // Fork a child process
    fork();

    return 0;
}
