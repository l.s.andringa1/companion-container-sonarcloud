package nl.rug.digitallab.themis.companion.integration.jackson

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeType
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import java.nio.file.Path
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi
import kotlin.io.path.exists
import kotlin.io.path.readBytes

class ExternalFileLoader(private val directory: Path) {
    fun walkTree(json: JsonNode): JsonNode {
        return when(json.nodeType) {
            JsonNodeType.STRING -> visitString(json)
            JsonNodeType.OBJECT -> visitObject(json.withObject(""))
            JsonNodeType.ARRAY -> visitArray(json.withArray(""))
            else -> json
        }
    }

    @OptIn(ExperimentalEncodingApi::class)
    private fun visitString(json: JsonNode): JsonNode {
        val content = json.asText()
        if(!content.startsWith("@"))
            return json

        // Lookup file
        val filename = content.removePrefix("@")
        val file = directory.resolve(filename)
        if(!file.exists())
            throw IllegalArgumentException("File $filename does not exist")

        // Read file as Base64 string
        return TextNode(Base64.encode(file.readBytes()))
    }

    private fun visitObject(json: ObjectNode): JsonNode {
        json.fieldNames().forEach { json.replace(it, walkTree(json[it])) }
        return json
    }

    private fun visitArray(json: ArrayNode): JsonNode {
        val size = json.size()
        for (i in 0..<size) {
            json.set(i, walkTree(json[i]))
        }
        return json
    }
}