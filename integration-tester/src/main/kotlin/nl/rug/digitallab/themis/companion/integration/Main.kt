package nl.rug.digitallab.themis.companion.integration

import io.quarkus.runtime.QuarkusApplication
import io.quarkus.runtime.annotations.QuarkusMain
import jakarta.inject.Inject
import nl.rug.digitallab.themis.companion.integration.testing.TestRunner

@QuarkusMain
class Main : QuarkusApplication {
    @Inject
    lateinit var runner: TestRunner

    @Throws(Exception::class)
    override fun run(vararg args: String): Int {
        runner.runTests()

        return 0
    }
}