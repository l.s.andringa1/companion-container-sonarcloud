val protobufVersion: String by rootProject
val jacksonProtobufVersion: String by rootProject

dependencies {
    implementation("io.quarkus:quarkus-jackson")

    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.hubspot.jackson:jackson-datatype-protobuf:$jacksonProtobufVersion")

    implementation("com.google.protobuf:protobuf-java-util:$protobufVersion")
}

