package nl.rug.digitallab.themis.companion.buildsrc

object TaskConfig {
    const val IMAGE_NAME = "companion-integration-environment:latest"
}