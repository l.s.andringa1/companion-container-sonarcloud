rootProject.name = "companion-container"

pluginManagement {
    val digitalLabGradlePluginVersion: String by settings
    plugins {
        id("nl.rug.digitallab.gradle.plugin.quarkus.project") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/projects/51717653/packages/maven") // Digital Lab Gradle Plugin
        gradlePluginPortal()
    }
}

include("companion-container")
include("integration-tester")
