tasks.compileKotlin {
    kotlinOptions {
        freeCompilerArgs += "-Xcontext-receivers"
    }
}

tasks.compileTestKotlin {
    kotlinOptions {
        freeCompilerArgs += "-Xcontext-receivers"
    }
}

val dockerJavaVersion: String by rootProject
val kotlinMockitoVersion: String by rootProject
val instancioVersion: String by rootProject
val coroutineTestVersion: String by rootProject
val jnaVersion: String by rootProject
val sequencedSocketVersion: String by rootProject
val commonQuarkusTelemetry: String by rootProject
val otelKotlinVersion: String by rootProject
val commonHelpersVersion: String by rootProject

dependencies {
    // Docker dependencies
    implementation("com.github.docker-java:docker-java:$dockerJavaVersion")
    implementation("com.github.docker-java:docker-java-transport-httpclient5:$dockerJavaVersion")
    implementation("com.github.docker-java:docker-java-transport-netty:$dockerJavaVersion")

    // Digital Lab dependencies
    implementation("nl.rug.digitallab.sequencedsocket:sequenced-socket:$sequencedSocketVersion")
    implementation("nl.rug.digitallab.common.quarkus:opentelemetry:$commonQuarkusTelemetry")
    implementation("nl.rug.digitallab.common.kotlin:helpers:$commonHelpersVersion")

    implementation("io.opentelemetry:opentelemetry-extension-kotlin:$otelKotlinVersion")
    implementation("io.quarkus:quarkus-hibernate-validator")

    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
    testImplementation("org.instancio:instancio-core:$instancioVersion")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutineTestVersion")
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
