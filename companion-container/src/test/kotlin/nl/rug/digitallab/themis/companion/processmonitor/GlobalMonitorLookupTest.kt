package nl.rug.digitallab.themis.companion.processmonitor

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests for [GlobalMonitorLookup].
 */
@QuarkusTest
class GlobalMonitorLookupTest {
    @Inject
    private lateinit var config: ProcessMonitorConfig

    @Test
    fun `The GlobalMonitorLookup can correctly store, retrieve and remove monitors`() {
        val monitor = ProcessMonitor(config)
        GlobalMonitorLookup.registerMonitor("test", monitor)
        assertEquals(monitor, GlobalMonitorLookup.getMonitor("test"))
        GlobalMonitorLookup.removeMonitor("test")
        assertEquals(null, GlobalMonitorLookup.getMonitor("test"))
    }
}
