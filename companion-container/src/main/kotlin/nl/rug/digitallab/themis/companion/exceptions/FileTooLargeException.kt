package nl.rug.digitallab.themis.companion.exceptions

/**
 * This exception is thrown when a file that is to be retrieved from the container is too large.
 */
class FileTooLargeException : Exception()