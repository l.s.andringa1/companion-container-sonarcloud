package nl.rug.digitallab.themis.companion.runner

import com.github.dockerjava.api.exception.NotFoundException
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.*
import kotlinx.coroutines.selects.select
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.quarkus.opentelemetry.withCoroutineSpan
import nl.rug.digitallab.themis.companion.processmonitor.GlobalMonitorLookup
import nl.rug.digitallab.themis.companion.runner.contexts.ContainerContext
import nl.rug.digitallab.themis.companion.runner.data.common.FileScheme
import nl.rug.digitallab.themis.companion.runner.data.request.ContainerConfiguration
import nl.rug.digitallab.themis.companion.util.docker.DockerManager
import nl.rug.digitallab.themis.companion.runner.data.request.ResourceLimits
import nl.rug.digitallab.themis.companion.runner.data.result.ContainerResult
import nl.rug.digitallab.themis.companion.runner.data.result.ContainerStatus
import nl.rug.digitallab.themis.companion.util.docker.callback.CompanionLogger
import org.jboss.logging.Logger

/**
 * A container executed by the companion container that executes something in the judgement pipeline.
 * The container runs as a docker container.
 */
@ApplicationScoped
class ContainerRunner {
    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var dockerManager: DockerManager

    @Inject
    private lateinit var fileHandler: ContainerFileHandler

    /**
     * Runs a container managed by the companion container.
     * This will create the container, manage all features implemented by the companion container, start the container, and clean up afterward.
     *
     * @param containerConfiguration The configuration for the container.
     */
    suspend fun runContainer(
        containerConfiguration: ContainerConfiguration
    ): ContainerResult = withCoroutineSpan("RUN container", Dispatchers.IO) {
        // Pull image
        if (!dockerManager.imageExists(containerConfiguration.image)) {
            dockerManager.pullImage(containerConfiguration.image)
        }

        return@withCoroutineSpan withContainer(containerConfiguration) {
            // Insert the input files into the container
            fileHandler.insertFiles(containerConfiguration.inputFiles.filter { it.scheme == FileScheme.FILE })

            // Attach stdin to the container.
            // Only allows for one file to be attached, which is the first stream file found.
            dockerManager.attachStdIn(
                containerConfiguration.inputFiles.firstOrNull { it.scheme == FileScheme.STREAM && it.path == "stdin" },
                containerId
            )

            // Start the container
            dockerManager.startContainer(containerId)

            // Add logger for stdout/stdin monitoring. Also handles resource limits
            val companionLogger = monitorOutput(containerConfiguration.resourceLimits)

            // Monitor WallTime to kill the container if it exceeds its allowed limit.
            // This task is also responsible for blocking the current function execution until the container is done.
            monitorWallTime(containerConfiguration.resourceLimits)

            // Update all the information about the container found in docker inspect.
            inspectContainer()

            // Retrieve the output files from the container
            val outputFiles = fileHandler.retrieveFiles(containerConfiguration.outputFileRequests)

            // Write down the amount of KB written to stdout and stderr
            resourceUsage.output = companionLogger.outputSize()

            logger.info("Container $containerId finished with status $containerExecStatus")

            // The process monitor will only be running in untrusted mode, as that is when gVisor is available
            if (!containerConfiguration.trusted)
                retrieveProcessMonitorResults(containerConfiguration)

            return@withContainer ContainerResult(
                status = containerExecStatus,
                exitCode = exitCode,
                files = listOf(
                    companionLogger.getStdout(),
                    companionLogger.getStderr(),
                ) + outputFiles,
                resourceUsage = resourceUsage
            )
        }
    }

    /**
     * This functions communicates with the process monitor, and uses it to adjust the status of the action.
     * This includes:
     * - Checking whether the container exceeded the user process limit sometime during its execution.
     */
    context(ContainerContext)
    suspend fun retrieveProcessMonitorResults(containerConfiguration: ContainerConfiguration) {
        val monitor = GlobalMonitorLookup.getMonitor(containerId)
            ?: throw IllegalStateException("No process monitor found for container $containerId")

        // Give the monitor 5 seconds to finish
        // It should normally already be done the moment the container finishes, but this is a safety measure
        monitor.awaitFinish()

        val maxUserProcesses = monitor.maxUserProcessCount
        if (maxUserProcesses > containerConfiguration.resourceLimits.userProcesses) {
            reportStatus(ContainerStatus.EXCEEDED_PROCESSES)
            logger.info("Container $containerId exceeded the user process limit. Max processes: $maxUserProcesses, limit: ${containerConfiguration.resourceLimits.userProcesses}")
        }

        // Set used user processes
        resourceUsage.maxProcesses = maxUserProcesses
        resourceUsage.createdProcesses = monitor.totalCreatedProcesses
    }

    /**
     * Injects a container context into the provided block.
     *
     * This container context represents a real container that is managed by the companion container.
     * This function will take care of creating and cleaning up the container.
     * One can assume that a container is available within the provided block, and that it will be cleaned up afterward.
     *
     * @param containerConfiguration The configuration for the container.
     * @param block The block to execute with the container context.
     */
    suspend fun withContainer (
        containerConfiguration: ContainerConfiguration,
        block: suspend context(ContainerContext) () -> ContainerResult,
    ): ContainerResult {
        // Create the container
        val containerContext = dockerManager.createContainer(containerConfiguration)

        try {
            return block(containerContext)
        } finally {
            // Clean up the container
            dockerManager.deleteContainer(containerContext.containerId)
        }
    }

    /**
     * Check the conditions under which the container exited and update its status and exit code accordingly.
     */
    context(ContainerContext)
    fun inspectContainer() {
        val containerInfo = dockerManager.inspectContainer(containerId)

        val exitCode = containerInfo.state.exitCodeLong
            ?: throw IllegalStateException("Container $containerId has no exit code")
        reportExitCode(exitCode.toInt())

        if (containerInfo.state.oomKilled == true) {
            reportStatus(ContainerStatus.EXCEEDED_MEMORY)
        } else if (exitCode != 0L) {
            reportStatus(ContainerStatus.CRASHED)
        }

        // Set the amount of disk space used by the container
        // May overflow as sizeRw is an int. Awaiting fix to docker-java to be merged.
        resourceUsage.disk = containerInfo.sizeRw.B
    }

    /**
     * Set up a callback to monitor stdout and stderr of a container to both log it, and check if it doesn't exceed the limit.
     *
     * @param resourceLimits The resource limits for the container. Such as time limits, memory limits and disk limits.
     *
     * @return The result of the function, containing:
     * 1. The logger to use for monitoring the container.
     * 2. A boolean indicating whether the output limit was exceeded. True if it was, false otherwise.
     */
    context(ContainerContext)
    fun monitorOutput(resourceLimits: ResourceLimits): CompanionLogger {
        val companionLogger = CompanionLogger(containerId, resourceLimits.maxOutput,
            onOutputExceeded = {
                logger.info("Container $containerId exceeded its output limit, terminating...")
                dockerManager.killContainer(containerId)
                reportStatus(ContainerStatus.EXCEEDED_STREAM_OUTPUT)
            })
        dockerManager.logContainer(companionLogger, containerId)

        return companionLogger
    }

    /**
     * Set up a coroutine to monitor the wall time of a container and kill it if it exceeds the limit.
     * This function will block until the container is done.
     * It will also mark the container as succeeded if it finished on time, or as exceeded the wall time if it was killed.
     *
     * @param resourceLimits The resource limits for the container. Such as time limits, memory limits and disk limits.
     */
    context(ContainerContext)
    suspend fun monitorWallTime(resourceLimits: ResourceLimits) = withCoroutineSpan("AWAIT container") {
        withContext(Dispatchers.IO) {
            val deferredStopped = async { containerStopped() }
            val deferredTime = async { timeExceeded(resourceLimits) }

            // Await for any of the two coroutines to finish.
            // So resume once either the container has stopped, or the wall time limit has been exceeded.
            val startTime = System.currentTimeMillis()
            val status = select {
                deferredStopped.onAwait { it }
                deferredTime.onAwait { it }
            }
            val endTime = System.currentTimeMillis()

            deferredTime.cancel()
            deferredStopped.cancel()

            resourceUsage.wallTimeMs = (endTime - startTime).toInt()
            reportStatus(status)
        }
    }

    /**
     * Wait for the wall time limit to be exceeded and kill the container if it is.
     * If the container is killed, mark it as having exceeded the wall time.
     *
     * @param resourceLimits The resource limits for the container. Such as time limits, memory limits and disk limits.
     */
    context(ContainerContext)
    suspend fun timeExceeded(resourceLimits: ResourceLimits): ContainerStatus {
        delay(resourceLimits.wallTime.toLong())

        return try {
            logger.info("Killing container $containerId")
            dockerManager.killContainer(containerId)

            ContainerStatus.EXCEEDED_WALL_TIME
        } catch (e: NotFoundException) {
            // Container already died. Likely to be a race condition where the container finished just before the monitor.
            // Let's give the student the benefit of the doubt
            logger.info("Container $containerId already died, assuming it finished just before/after the wall time limit was exceeded." +
                    "Marking container as not having exceeded the time.")

            ContainerStatus.SUCCEEDED
        }
    }

    /**
     * Wait for the container to stop.
     * If the container stops, mark it as succeeded.
     */
    context(ContainerContext)
    suspend fun containerStopped(): ContainerStatus = withContext(Dispatchers.IO) {
        dockerManager.waitContainer(containerId)

        return@withContext ContainerStatus.SUCCEEDED
    }
}
