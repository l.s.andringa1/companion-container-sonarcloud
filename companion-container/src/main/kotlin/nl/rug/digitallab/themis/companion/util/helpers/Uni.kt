package nl.rug.digitallab.themis.companion.util.helpers

import io.opentelemetry.context.Context
import io.opentelemetry.extension.kotlin.asContextElement
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.coroutines.uni
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
fun <T> contextualUni(context: CoroutineContext = EmptyCoroutineContext, suspendSupplier: suspend () -> T): Uni<T> =
    uni(CoroutineScope(context + Context.current().asContextElement()), suspendSupplier)
