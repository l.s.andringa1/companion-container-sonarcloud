package nl.rug.digitallab.themis.companion.services

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping

@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.companion-container.services")
interface ServicesConfig {
    fun socketPath(): String
}
