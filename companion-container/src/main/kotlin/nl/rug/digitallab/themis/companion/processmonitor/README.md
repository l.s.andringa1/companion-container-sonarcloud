# Process monitor
This folder contains the implementation for the process monitor, the subsystem of the companion responsible for monitoring the processes running within the container.
Responsibilities include:
- Tracking number of running processes
- Preventing too many processes from running
- Detecting zombie and orphan processes (not yet implemented)
