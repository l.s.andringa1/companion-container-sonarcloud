package nl.rug.digitallab.themis.companion.runner.data.result

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.protospec.judgement.v1.ResourceUsage as ProtoResourceUsage

/**
 * The resources used by an execution.
 * This includes wall time, memory, disk, etc.
 */
class ResourceUsage{
    var wallTimeMs: Int? = null
    var memory: ByteSize? = null
    var disk: ByteSize? = null
    var output: ByteSize? = null
    var createdProcesses: Int? = null // Only available on gVisor
    var maxProcesses: Int? = null     // Only available on gVisor

    /**
     * Convert this object to its protobuf equivalent.
     *
     * @return The protobuf equivalent of this object.
     */
    fun toProto(): ProtoResourceUsage {
        val resourceUsage = ProtoResourceUsage.newBuilder()

        wallTimeMs?.let { resourceUsage.setWallTimeMs(it) }
        memory?.let { resourceUsage.setMemoryBytes(it.B) }
        disk?.let { resourceUsage.setDiskBytes(it.B) }
        output?.let { resourceUsage.setOutputBytes(it.B) }
        createdProcesses?.let { resourceUsage.setCreatedProcesses(it) }
        maxProcesses?.let { resourceUsage.setMaxProcesses(it) }

        return resourceUsage.build()
    }
}
